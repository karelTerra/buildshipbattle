﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HeartPointsDisplay : MonoBehaviour
{
	[SerializeField]
	protected Ship ship;

	[SerializeField]
	protected TextMeshProUGUI heartPointsValueText;

	private void Start ()
	{
		ship.HeartPointsValueChangedAction += OnHeartPointsValueChanged;
	}

	private void OnHeartPointsValueChanged (int total)
	{
		heartPointsValueText.text = total.ToString ();
	}
}
