﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
	[SerializeField]
	protected Heart heart;

	public Action<int> HeartPointsValueChangedAction;

	protected int heartPoints;

	private void Start ()
	{
		heart.HeartBeatAction += OnHeartBeat;
		heart.StartHeartBeat ();
	}

	private void OnHeartBeat ()
	{
		heartPoints++;

		if(HeartPointsValueChangedAction != null)
		{
			HeartPointsValueChangedAction (heartPoints);
		}
	}
}
