﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (SpriteSelector))]
public class Player : MonoBehaviour
{
	[SerializeField]
	protected GameUIManager gameUIManager;

	protected SpriteSelector spriteSelector;

	private void Awake ()
	{
		spriteSelector = GetComponent<SpriteSelector> ();
		spriteSelector.SpriteSelectedAction += OnSpriteSelectedAction;
	}

	private void OnSpriteSelectedAction (GameObject clicked)
	{
		if (clicked != null)
		{
			Debug.Log ("Sprite Selected: " + clicked.name);
			gameUIManager.ShowUpgradePannel ();
		}
		else
		{
			gameUIManager.HideUpgradePannel ();
		}
	}
}
