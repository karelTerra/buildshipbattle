﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipPart : MonoBehaviour
{
	[SerializeField]
	protected ShipParts part;

	[SerializeField]
	protected ShipPartScriptableObject shipPartScriptable;

	public Action<GameObject> PartDestroyedAction;

	public float Health { get; protected set; }
	public ShipParts Part { get { return part; } }

	public void TakeDamage (float damage)
	{
		Health -= damage;

		if(Health <= 0f)
		{
			if(PartDestroyedAction != null)
			{
				PartDestroyedAction (gameObject);
			}
		}
	}
}
