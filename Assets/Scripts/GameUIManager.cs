﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUIManager : MonoBehaviour
{
	[SerializeField]
	protected Animator upgradePannelAnimator;

    public void ShowUpgradePannel()
	{
		upgradePannelAnimator.SetBool ("UpgradePanelVisible", true);
	}

	public void HideUpgradePannel ()
	{
		upgradePannelAnimator.SetBool ("UpgradePanelVisible", false);
	}
}
