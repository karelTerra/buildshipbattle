﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour
{
	[SerializeField]
	protected float heartRateBPS;

	[SerializeField]
	protected Animator heartAnimator;

	public Action HeartBeatAction;

	private Coroutine heartBeatRoutine;

	private float heartRateTime { get { return 1f / heartRateBPS; } }
	private bool isHeartBeating;

	public void StartHeartBeat ()
	{
		heartAnimator.SetBool ("HeartBeat", true);
	}

	public void StopHeartBeat ()
	{
		heartAnimator.SetBool ("HeartBeat", false);
	}

	public void IncreaseHeartRate (float amount)
	{
		heartRateBPS += amount;
	}

	protected void HeartBeat ()
	{
		if (HeartBeatAction != null)
		{
			HeartBeatAction ();
		}
	}
}