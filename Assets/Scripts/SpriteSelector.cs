﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteSelector : MonoBehaviour
{
	public Action<GameObject> SpriteSelectedAction;


	public void Update ()
	{
		if (Input.GetMouseButtonDown (1))
		{
			RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);

			if (hit)
			{
				if (SpriteSelectedAction != null)
				{
					SpriteSelectedAction (hit.collider.gameObject);
				}
			}
			else
			{
				if (SpriteSelectedAction != null)
				{
					SpriteSelectedAction (null);
				}
			}
		}
	}
}
