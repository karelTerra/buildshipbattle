﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShipParts
{
	Hull,
	Gun,
	Lazer,
	Hook,
	Rockets,
	Engine,
}

public enum PartTiers
{
	Decrepid,
	Broken,
	Patched,
	Good,
	Polished,
	Reenforced,
	Robust,
	Perfect,
}

[CreateAssetMenu (fileName = "ShipPartScriptableObject", menuName = "ScriptableObjects/ShipParts")]
public class ShipPartScriptableObject : ScriptableObject
{
	[SerializeField]
	protected ShipPartData [] shipParts;
	
	public float GetPartHealth(ShipParts part, PartTiers tier)
	{
		float health = -1f;

		for (int i = 0; i < shipParts.Length; i++)
		{
			if(shipParts[i].part == part && shipParts[i].tier == tier)
			{
				health = shipParts [i].Health; 
				break;
			}
		}

		return health;
	}

	public int GetPartUpgradeCost(ShipParts part, PartTiers tier)
	{
		int price = -1;

		for (int i = 0; i < shipParts.Length; i++)
		{
			if (shipParts [i].part == part && shipParts [i].tier == tier)
			{
				price = shipParts [i].Cost;
				break;
			}
		}

		return price;
	}
}

[System.Serializable]
public struct ShipPartData
{
	public ShipParts part;
	public PartTiers tier;
	public float Health;
	public int Cost;
}
